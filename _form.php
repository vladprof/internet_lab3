<?php
/**
 * Created by PhpStorm.
 * User: Duckson
 * Date: 16.03.2019
 * Time: 11:35
 */
/** @var $form_type string */
?>
<h2><?= $form_type == 'login' ? 'Вход' : 'Регистрация' ?></h2>
<form action="" method="post">
    <div class="form-group">
        <label for="exampleInputEmail1">Логин</label>
        <input name="login" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
               placeholder="Введите логин">
        <small class="form-text text-muted">Не более 50 символов</small>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Пароль</label>
        <input name="password" type="password" class="form-control" id="exampleInputPassword1"
               placeholder="Введите пароль">
        <small class="form-text text-muted">Не более 50 символов</small>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
