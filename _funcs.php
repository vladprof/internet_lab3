<?php
/**
 * Created by PhpStorm.
 * User: Duckson
 * Date: 16.03.2019
 * Time: 12:21
 */

/**@param $url string
 * @param $statusCode int
 */
function redirect($url, $statusCode = 303)
{
    header('Location: ' . $url, true, $statusCode);
    die();
}

function checkCookie(){
    if (!isset($_COOKIE["user"])) {
        redirect('/lab3');

    } elseif(!$_COOKIE["user"]){
        redirect('/lab3');
    }
}