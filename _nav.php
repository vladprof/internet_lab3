<?php
/**
 * Created by PhpStorm.
 * User: Duckson
 * Date: 16.03.2019
 * Time: 12:00
 */
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="content-margin container-fluid">
        <a class="navbar-brand" href="/lab3">Лаба 3</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/lab3">Логин</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/lab3/register.php">Регистрация</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/lab3/gallery/index.php">Галерея</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
