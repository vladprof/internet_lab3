<?php
/**
 * Created by PhpStorm.
 * User: Duckson
 * Date: 16.03.2019
 * Time: 12:23
 */
require '../_funcs.php';
checkCookie();
$message = '';
if ($_POST && isset($_POST['image_for_deletion'])) {
    unlink('images/' . $_POST['image_for_deletion']);
} elseif ($_POST && isset($_POST['submit'])) {
    [$name, $ext] = explode('.', $_FILES["image"]["name"]);
    $target_dir = "images/";
    $target_file = $target_dir . $name . "_&&_${_COOKIE['user']}_&&_" . date('YmdHms') . '.' . $ext;
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    /*
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if ($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
    if ($_FILES["fileToUpload"]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
     */
    if (file_exists($target_file)) {
        $message = "<div class=\"alert alert-danger\" role=\"alert\">
                        Sorry, file already exists.
                    </div>";
        $uploadOk = 0;
    }
    if ($uploadOk == 0) {
        $message = "<div class=\"alert alert-danger\" role=\"alert\">
                        Sorry, your file was not uploaded.
                    </div>";
    } else {
        if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
            $message = "<div class=\"alert alert-success\" role=\"alert\">
                        The file " . basename($_FILES["image"]["name"]) . " has been uploaded.
                    </div>";
        } else {
            $message = "<div class=\"alert alert-danger\" role=\"alert\">
                        Sorry, there was an error uploading your file.
                    </div>";
        }
    }
}
$images = [];
foreach (scandir('images', 1) as $image) {
    if ($image != '.' && $image != '..' && explode('_&&_', $image)[1] == $_COOKIE['user']) {
        $images[] = $image;
    }
}
?>

<head>
    <?php require '../_deps.php'; ?>
    <script src="js/gallery.js"></script>
</head>
<?php require '../_nav.php'; ?>
<div class="content-margin">
    <?= $message ?>
    <div class="container-fluid row">
        <div class="col-sm-6">
            <form action="" method="post" id="gallery_form" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="exampleFormControlFile1"><b>Изображение</b></label>
                    <input name="image" type="file" class="form-control-file" id="image">
                </div>
                <input id="submit" type="submit" name="submit" value="Submit" class="btn btn-primary" disabled>
            </form>
        </div>
        <div class="col-sm-6">
            <b>Предпросмотр</b>
            <img id="preview" class="img-fluid" src="">
        </div>
    </div>
    <?php if ($images): ?>
        <div class="bd-example card">
            <div id="carouselExampleIndicators" class="carousel slide container" data-ride="carousel">
                <ol class="carousel-indicators">
                    <?php foreach ($images as $id => $image): ?>
                        <li data-target="#carouselExampleIndicators"
                            data-slide-to="<?= $id ?>" <?= $id == 0 ? 'class="active"' : '' ?>></li>
                    <?php endforeach; ?>
                </ol>
                <div class="carousel-inner">
                    <?php foreach ($images as $id => $image): ?>
                        <div class="carousel-item <?= $id == 0 ? 'active' : '' ?>">
                            <img class="d-block w-100" src="images/<?= $image ?>" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <form action="" method="post">
                                    <input type="hidden" name="image_for_deletion" value="<?= $image ?>">
                                    <button class="btn btn-danger">X</button>
                                </form>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    <?php endif; ?>
</div>

