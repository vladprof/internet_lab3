<?php
/**
 * Created by PhpStorm.
 * User: Duckson
 * Date: 16.03.2019
 * Time: 11:07
 */
require '_funcs.php';
if (isset($_COOKIE["user"])){
    setcookie("user", "", time() - 3600);
    redirect('gallery/index.php');
}
$form_type = 'login';
$message = '';
if ($_POST && $_POST['login'] && $_POST['password']) {
    $kostyl = fopen('1.txt', 'a+');
    $data = explode(PHP_EOL, fread($kostyl, filesize('1.txt')));
    $can_login = false;
    foreach ($data as $entry){
        $entry = explode(';', $entry);
        if($entry[0] == $_POST['login']){
            $can_login = true;
            break;
        }
    }
    if ($can_login) {
        if (!isset($_COOKIE["user"]) || (isset($_COOKIE["user"]) && !$_COOKIE["user"])) {
            setcookie("user", $_POST['login'], (time() + 60 * 60 * 24 * 30));
        }
        redirect('gallery/index.php');
    } else {
        $message = '<div class="alert alert-danger" role="alert">
                        Неверный логин\пароль!
                    </div>';
    }
}

?>
<html>
<head>
    <?php require '_deps.php'; ?>
</head>
<?php require '_nav.php'; ?>

<body>
<div class="content-margin">
    <?= $message ?>
    <div class="container-fluid">
        <?php require '_form.php' ?>
    </div>
</div>
</body>
</html>
