<?php
/**
 * Created by PhpStorm.
 * User: Duckson
 * Date: 16.03.2019
 * Time: 11:07
 */
require '_funcs.php';
$message = '';
$form_type = 'register';
if ($_POST && $_POST['login'] && $_POST['password']) {
    $kostyl = fopen('1.txt', 'a+');
    $data = explode(PHP_EOL, fread($kostyl, filesize('1.txt')));
    $can_register = true;
    foreach ($data as $entry) {
        $entry = explode(';', $entry);
        if ($entry[0] == $_POST['login']) {
            $can_register = false;
            break;
        }
    }
    if ($can_register) {
        fwrite($kostyl, $_POST['login'] . ';' . $_POST['password'] . PHP_EOL );
        redirect('index.php');
    } else {
        $message = '<div class="alert alert-danger" role="alert">
                        Пользователь с таким логином уже зарегистрирован!
                    </div>';
    }
    fclose($kostyl);
}

?>
<head>
    <?php require '_deps.php'; ?>
</head>
<?php require '_nav.php'; ?>
<div class="content-margin">
    <?= $message ?>
    <div class="container-fluid">
        <?php require '_form.php' ?>
    </div>
</div>
